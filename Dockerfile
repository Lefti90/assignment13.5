# To build:
# docker build --build-arg PORT=3000 --build-arg PG_HOST=172.17.0.2 --build-arg PG_PORT=5432 --build-arg PG_USERNAME=pguser --build-arg PG_PASSWORD=pgpass --build-arg PG_DATABASE=postgres -t products-api .
#
# To run:
# docker run -it -p 3000:3000 products-api

FROM node:18-alpine AS builder

COPY ./*.json ./
COPY ./src ./src

RUN npm ci
RUN npm run build

FROM node:18-alpine AS final

WORKDIR /app

ARG PORT
ARG PG_HOST
ARG PG_PORT
ARG PG_USERNAME
ARG PG_PASSWORD
ARG PG_DATABASE

ENV PORT=${PORT}
ENV PG_HOST=${PG_HOST}
ENV PG_PORT=${PG_PORT}
ENV PG_USERNAME=${PG_USERNAME}
ENV PG_PASSWORD=${PG_PASSWORD}
ENV PG_DATABASE=${PG_DATABASE}
ENV NODE_ENV=production

COPY --from=builder ./dist ./dist
COPY ./package*.json ./
RUN npm ci --production

EXPOSE ${PORT}
CMD ["npm", "start"]